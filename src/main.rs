use plonky2::{plonk::{circuit_data::{CircuitConfig, CircuitData}, circuit_builder::CircuitBuilder, config::{GenericConfig, PoseidonGoldilocksConfig}}, iop::{target::Target, witness::{PartialWitness, Witness}}, hash::{hash_types::{HashOutTarget, HashOut}, poseidon::PoseidonHash, merkle_proofs::MerkleProofTarget, merkle_tree::MerkleTree}};
use plonky2::field::types::Field;
use plonky2::plonk::config::Hasher;

const PREIMAGE_LEN: usize = 16;
const CAP_HEIGHT: usize = 0;
const D: usize = 2;
const MERKLE_PATH_LEN: usize = 20;
const SECRET_SIZE: usize = 4;
type C = PoseidonGoldilocksConfig;
type H = <C as GenericConfig<D>>::Hasher;
type F = <C as GenericConfig<D>>::F;

struct InputTargets {
    preimage: Vec<Target>,
    hash: HashOutTarget
}

/// Dummy circuit that receives a hash as a public input and checks that the prover knows the preimage of the public hash
fn generate_circuit(
    config: &CircuitConfig,
) -> (CircuitData<F, C, D>, InputTargets) {
    let mut builder = CircuitBuilder::<F, D>::new(config.clone());

    // Hash public input
    let hash = builder.add_virtual_hash();
    builder.register_public_inputs(&hash.elements);

    // Compute the hash
    let preimage = builder.add_virtual_targets(PREIMAGE_LEN);
    let computed_hash =
        builder.hash_n_to_hash_no_pad::<<C as GenericConfig<D>>::Hasher>(preimage.clone());

    builder.connect_hashes(computed_hash, hash);

    let input_targets = InputTargets {
        preimage,
        hash
    };

    // Building the circuit
    (builder.build::<C>(), input_targets)
}

struct MerkleInputTargets {
    merkle_root: HashOutTarget,
    merkle_proof: MerkleProofTarget,
    leaf_index: Target,
    leaf: Vec<Target>
}

/// Dummy circuit that checks if a use knows a leaf in a merkle-tree
fn generate_merkle_circuit(
    config: &CircuitConfig,
) -> (CircuitData<F, C, D>, MerkleInputTargets) {
    // Starting to build the circuit with inputs
    let mut builder = CircuitBuilder::<F, D>::new(config.clone());

    let merkle_root = builder.add_virtual_hash();
    // Register the root of the merkle tree as public input
    builder.register_public_inputs(&merkle_root.elements);

    let merkle_proof = MerkleProofTarget {
        siblings: builder.add_virtual_hashes(MERKLE_PATH_LEN),
    };

    let leaf_index = builder.add_virtual_target();
    let i_bits = builder.split_le(leaf_index, MERKLE_PATH_LEN);

    let leaf = builder.add_virtual_targets(SECRET_SIZE);

    // Verify the merkle proof
    builder.verify_merkle_proof::<<C as GenericConfig<D>>::InnerHasher>(
        leaf.clone(),
        &i_bits,
        merkle_root,
        &merkle_proof,
    );

    let input_targets = MerkleInputTargets {
        merkle_root,
        merkle_proof,
        leaf_index,
        leaf
    };

    // Building the circuit
    (builder.build::<C>(), input_targets)
}

/// Here we generate the circuit twice, we get the same circuit data and then verification always works
/// Also, the circuit data is always exactly the same even if we run the process multiple times.
pub fn use_circuit_in_two_different_places() {
    let config = CircuitConfig::standard_recursion_zk_config();
    let (circuit_data1, input_targets1) = generate_circuit(&config);
    println!("Circuit data prover vo:\n{:?}", circuit_data1.verifier_only);
    println!("Circuit data prover cm:\n{:?}", circuit_data1.common);

    // Use the circuit to prove knowledge of pre-image
    // Compute pre-image
    let preimage = [F::ONE; PREIMAGE_LEN];
    let hash_out: HashOut<F> = PoseidonHash::hash_no_pad(&preimage);

    // Create partial witness
    let mut pw = PartialWitness::new();
    pw.set_hash_target(input_targets1.hash, hash_out);
    for i in 0..PREIMAGE_LEN {
        pw.set_target(input_targets1.preimage[i], preimage[i]);
    }

    // Generate proof
    let proof = circuit_data1.prove(pw).unwrap();

    // Now we would like to verify the proof on a different device
    let (circuit_data2, _input_targets2) = generate_circuit(&config);
    println!("Circuit data verify vo:\n{:?}", circuit_data2.verifier_only);
    println!("Circuit data verify cm:\n{:?}", circuit_data2.common);
    circuit_data2.verify(proof).unwrap();
    println!("Proof verified!");
}

fn generate_merkle_tree(
    tree_size: usize,
    leaf: Vec<F>,
    index: usize,
) -> MerkleTree<F, H> {
    let mut leaves: Vec<Vec<F>> = (0..tree_size).map(|_| F::rand_vec(SECRET_SIZE)).collect();
    leaves[index] = leaf;
    MerkleTree::<F, H>::new(leaves, CAP_HEIGHT)
}

/// Simulate running the circuit data generation on 2 different devices, about half of the time the verification fails
/// Whenever the circuitdata is the same, the verification holds, when it is different it fails.
pub fn use_merkle_circuit_in_two_different_places() {
    let config = CircuitConfig::standard_recursion_zk_config();
    let (circuit_data1, input_targets1) = generate_merkle_circuit(&config);
    println!("Circuit data prover vo:\n{:?}", circuit_data1.verifier_only);
    println!("Circuit data prover cm:\n{:?}", circuit_data1.common);

    // Use the circuit to prove knowledge of pre-image
    // Compute pre-image
    let leaf = F::rand_vec(SECRET_SIZE);
    let index = 7;
    let merkle_tree = generate_merkle_tree(1 << MERKLE_PATH_LEN, leaf.clone(), index);
    let merkle_proof = merkle_tree.prove(index);

    // Create partial witness
    let mut pw = PartialWitness::new();
    pw.set_hash_target(input_targets1.merkle_root, merkle_tree.cap.0[0]);
    
    for i in 0..merkle_proof.siblings.len() {
        pw.set_hash_target(input_targets1.merkle_proof.siblings[i], merkle_proof.siblings[i]);
    }

    pw.set_target(input_targets1.leaf_index, F::from_canonical_usize(index));

    for i in 0..SECRET_SIZE {
        pw.set_target(input_targets1.leaf[i], leaf[i]);
    }

    // Generate proof
    let proof = circuit_data1.prove(pw).unwrap();

    // Now we would like to verify the proof on a different device
    let (circuit_data2, _input_targets2) = generate_merkle_circuit(&config.clone());
    println!("Circuit data verify vo:\n{:?}", circuit_data2.verifier_only);
    println!("Circuit data verify cm:\n{:?}", circuit_data2.common);
    circuit_data2.verify(proof).unwrap();
    println!("Proof verified!");
}


pub fn compare_circuit_data() {
    // Bug occurs with both configs
    let config = CircuitConfig::standard_recursion_config();
    // let config = CircuitConfig::standard_recursion_zk_config();
    let mut true_count = 0usize;
    let mut false_count = 0usize;
    for i in 0..100 {
        println!("iteration: {:?}", i);
        let (circuit_data1, _) = generate_merkle_circuit(&config.clone());
        let (circuit_data2, _) = generate_merkle_circuit(&config.clone());
        // let (circuit_data1, _) = generate_circuit(&config.clone());
        // let (circuit_data2, _) = generate_circuit(&config.clone());
        if format!("{:?}", circuit_data1.common) == format!("{:?}", circuit_data2.common) && format!("{:?}", circuit_data1.verifier_only) == format!("{:?}", circuit_data2.verifier_only) {
            true_count += 1;
        }
        else {
            false_count += 1;
        }
        println!("True: {:?}, False: {:?}", true_count, false_count);
    }
}


fn main() {
    use_circuit_in_two_different_places();
    use_merkle_circuit_in_two_different_places();
    compare_circuit_data();
}
